﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace Atlassian.Jira.OAuth
{
    [Serializable]
    [DataContract]
    public class JiraOrganization
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public IEnumerable<string> Scopes { get; set; }

        [DataMember]
        public string AvatarUrl { get; set; }
    }
}
