﻿using System;
using System.Runtime.Serialization;

namespace Atlassian.Jira.OAuth
{
    [Serializable]
    [DataContract]
    public class ErrorResponse
    {
        [DataMember]
        public virtual string Error { get; set; }

        [DataMember]
        public virtual string ErrorDescription { get; set; }
    }
}
