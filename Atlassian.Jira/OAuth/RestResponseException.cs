﻿using System;
using System.Net;
using System.Runtime.Serialization;

namespace Atlassian.Jira.OAuth
{
    /// <summary>
    /// Represents and error that occured within the API.
    /// </summary>
    [Serializable]
    public class RestResponseException
        : Exception
    {
        /// <summary>
        /// The details of the error from the API.
        /// </summary>
        [DataMember]
        public ErrorResponse ErrorDetails { get; private set; }

        public RestResponseException(HttpStatusCode statusCode, ErrorResponse errorDetails)
            : this(statusCode)
        {
            ErrorDetails = errorDetails;
        }

        public RestResponseException()
        {
        }

        public RestResponseException(string message) : base(message)
        {
        }

        public RestResponseException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected RestResponseException(System.Runtime.Serialization.SerializationInfo serializationInfo, System.Runtime.Serialization.StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }

        /// <summary>
        /// The <see cref="T:System.Net.HttpStatusCode"/> returned by the server.
        /// 
        /// </summary>
        [DataMember]
        public HttpStatusCode StatusCode { get; private set; }

        /// <summary>
        /// The details of the error from the API.
        /// 
        /// </summary>
        [DataMember]
        public RestErrorDetails RestDetails { get; private set; }

        /// <summary>
        /// Instantiates a new instance of the <see cref="T:RestResponseException"/> class providing the <paramref name="statusCode"/>.
        /// 
        /// </summary>
        public RestResponseException(HttpStatusCode statusCode)
        {
            StatusCode = statusCode;
        }

        /// <summary>
        /// Instantiates a new instance of the <see cref="T:RestResponseException"/> class providing the <paramref name="restDetails"/>.
        /// 
        /// </summary>
        public RestResponseException(RestErrorDetails restDetails)
          : this(restDetails.StatusCode)
        {
            RestDetails = restDetails;
        }

        /// <summary>
        /// Further details of the error
        /// 
        /// </summary>
        public class RestErrorDetails
        {
            /// <summary>
            /// The <see cref="T:System.Net.HttpStatusCode"/> returned by the server.
            /// 
            /// </summary>
            public HttpStatusCode StatusCode { get; set; }

            /// <summary>
            /// The message returned from the server.
            /// 
            /// </summary>
            public string Message { get; set; }

            /// <summary>
            /// Any further details returned from the server.
            /// 
            /// </summary>
            public string Details { get; set; }
        }
    }
}
