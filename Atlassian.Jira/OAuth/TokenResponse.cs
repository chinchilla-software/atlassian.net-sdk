﻿using System;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Atlassian.Jira.OAuth
{
    [Serializable]
    [DataContract]
    public class TokenResponse
    {
        /// <summary>
        /// The access token
        /// </summary>
        [DataMember]
        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }

        /// <summary>
        /// The refresh token if OAuth. NULL if JWT
        /// </summary>
        [DataMember]
        [JsonProperty(PropertyName = "refresh_token")]
        public string RefreshToken { get; set; }

        /// <summary>
        /// The time until this token will expire expressed in seconds. Most likely 15 minutes.
        /// </summary>
        [DataMember]
        [JsonProperty(PropertyName = "expires_in")]
        public long ExpiresIn { get; set; }

        /// <summary>
        /// Should equal "Bearer" if JWT. NULL if OAuth
        /// </summary>
        [DataMember]
        [JsonProperty(PropertyName = "token_type")]
        public string TokenType { get; set; }

        /// <summary>
        /// Allowed scopes if OAuth. NULL if JWT
        /// </summary>
        [DataMember]
        [JsonProperty(PropertyName = "scope")]
        public string Scope { get; set; }
    }
}
