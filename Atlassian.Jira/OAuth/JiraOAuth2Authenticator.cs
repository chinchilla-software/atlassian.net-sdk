﻿using System;
using System.Collections.Generic;
using System.Net;

using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;

namespace Atlassian.Jira.OAuth
{
    public class JiraOAuth2Authenticator : OAuth2AuthorizationRequestHeaderAuthenticator
    {
        public JiraOAuth2Authenticator(string accessToken)
            : this(accessToken, "Bearer")
        {
        }

        protected JiraOAuth2Authenticator(string refreshToken, string accessToken, string tokenType = "Bearer")
            : this(accessToken, tokenType)
        {
            RefreshToken = refreshToken;
        }

        public JiraOAuth2Authenticator(string accessToken, string tokenType)
            : base(accessToken, tokenType)
        {
            AccessToken = accessToken;
        }

        public string RefreshToken { get; protected set; }

        public string AccessToken { get; protected set; }

        /// <remarks>
        /// See https://developer.atlassian.com/cloud/jira/platform/oauth-2-3lo-apps/#3-1-get-the-cloudid-for-your-site.
        /// </remarks>
        public IList<JiraOrganization> CloudOrganizations { get; protected set; }

        /// <remarks>
        ///See https://developer.atlassian.com/cloud/jira/platform/oauth-2-3lo-apps/#use-a-refresh-token-to-get-another-access-token-and-refresh-token-pair
        /// </remarks>
        public static IAuthenticator GenerateAccessTokenFromRefreshToken(string clientId, string clientSecret, string callbackUrl, string refreshToken)
        {
            TokenResponse tokens = GetToken(clientId, clientSecret, callbackUrl, refreshToken: refreshToken);
            var result = new JiraOAuth2Authenticator(tokens.RefreshToken, tokens.AccessToken, "Bearer");
            result.GetCloudOrganizations();
            return result;
        }

        /// <remarks>
        ///See https://developer.atlassian.com/cloud/jira/platform/oauth-2-3lo-apps/#2--exchange-authorization-code-for-access-token
        /// </remarks>
        public static IAuthenticator GenerateAccessTokenFromAuthorizationCode(string clientId, string clientSecret, string callbackUrl, string authorizationCode)
        {
            TokenResponse tokens = GetToken(clientId, clientSecret, callbackUrl, authorizationCode: authorizationCode);
            var result = new JiraOAuth2Authenticator(tokens.RefreshToken, tokens.AccessToken, "Bearer");
            result.GetCloudOrganizations();
            return result;
        }

        static TokenResponse GetToken(string clientId, string clientSecret, string callbackUrl, string authorizationCode = null, string refreshToken = null)
        {
            var client = new RestClient("https://auth.atlassian.com");
            var request = new RestRequest("/oauth/token", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddParameter("client_id", clientId, ParameterType.GetOrPost);
            request.AddParameter("client_secret", clientSecret, ParameterType.GetOrPost);
            if (!string.IsNullOrWhiteSpace(authorizationCode))
            {
                request.AddParameter("grant_type", "authorization_code", ParameterType.GetOrPost);
                request.AddParameter("code", authorizationCode, ParameterType.GetOrPost);
            }
            else
            {
                request.AddParameter("grant_type", "refresh_token", ParameterType.GetOrPost);
                request.AddParameter("refresh_token", refreshToken, ParameterType.GetOrPost);
            }
            request.AddParameter("redirect_uri", callbackUrl, ParameterType.GetOrPost);
            IRestResponse response = client.Execute(request);
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    var newToken = JsonConvert.DeserializeObject<TokenResponse>(response.Content);
                    return newToken;
                default:
                    var errorContent = JsonConvert.DeserializeObject<ErrorResponse>(response.Content);
                    throw new RestResponseException(response.StatusCode, errorContent);
            }
        }

        protected virtual void GetCloudOrganizations()
        {
            var orgClient = new RestClient("https://api.atlassian.com");
            var orgRequest = new RestRequest($"/oauth/token/accessible-resources", Method.GET);
            orgRequest.RequestFormat = DataFormat.Json;
            orgRequest.AddHeader("Authorization", "Bearer " + AccessToken);

            var orgResponse = orgClient.Execute(orgRequest);
            switch (orgResponse.StatusCode)
            {
                case HttpStatusCode.OK:
                    CloudOrganizations = JsonConvert.DeserializeObject<IList<JiraOrganization>>(orgResponse.Content);
                    break;
                default:
                    try
                    {
                        var exception = JsonConvert.DeserializeObject<Exception>(orgResponse.Content);
                        throw new RestResponseException(orgResponse.StatusCode, new ErrorResponse { Error = exception.Message });
                    }
                    catch
                    {
                        throw new RestResponseException("There was a problem collecting cloud orgnizations.");
                    }
            }
        }
    }
}
